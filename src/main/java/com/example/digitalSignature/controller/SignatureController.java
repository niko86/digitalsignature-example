package com.example.digitalSignature.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.core.io.ClassPathResource;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.signatures.PdfSigner;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/signature")
public class SignatureController {

	@GetMapping()
    public ResponseEntity<String> getController () {
		
		
		
		String src = new ClassPathResource("/static/PruebaPdf.pdf").getPath();
//		String dest = new ClassPathResource("/static/PruebaPdfDestino.pdf").getPath();
		String dest = "src/main/resources/static/PruebaPdfDest.pdf";

		try {
//			Para leer el pdf (tambien para escribir si se agrega un PdfWriter, como por ejemplo:
//				PdfDocument pdf = new PdfDocument(new PdfReader(src), new PdfWriter(dest));
			PdfDocument pdf = new PdfDocument(new PdfReader(src));
//			Para firmar el documento
			PdfSigner signer = new PdfSigner(new PdfReader(src),new FileOutputStream(dest), true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  new ResponseEntity("Todo OK! " + src, HttpStatus.OK);
    }
}
